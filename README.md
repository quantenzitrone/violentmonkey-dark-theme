# violentmonkey-dark-theme

Custom CSS Style for the Violentmonkey Browser Extension that makes the local extension website and its code editor dark themed, white on black to be specific.

The Style is appliable under Settings->Advanced->Custom Style in the Violentmonkey Settings.

After making sure it doesn't contain anything malicious, just copy the code from main.css in there and click on save.